# Oasis
A modified version of Closedverse/Oasis

# Requirements
```console
$ pip3 install Django urllib3 lxml passlib bcrypt pillow django-markdown-deux django-markdown2
```
  * Python 3.6.2?
  * Django
  * urllib3?
  * lxml
  * passlib
  * bcrypt
  * mysqlclient if you're using mysql
  * pillow
  * imghdr?
  * markdown

[![forthebadge](https://forthebadge.com/images/badges/made-with-python.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/you-didnt-ask-for-this.svg)](https://forthebadge.com)

[![forthebadge](https://forthebadge.com/images/badges/fuck-it-ship-it.svg)](https://forthebadge.com)
# Usage
It's a standard Django app.

Make the config, migrate, then either `runserver` or run it in uWSGI.
If you don't know how to set up a Django app, look it up please.

# Copyright
Copyright 2017 Arian Kordi, all rights reserved to their respective owners. (Nintendo, Hatena Co Ltd.)
